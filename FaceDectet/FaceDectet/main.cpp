/**
 * eye-tracking.cpp:
 * Eye detection and tracking with OpenCV
 *
 * This program tries to detect and tracking the user's eye with webcam.
 * At startup, the program performs face detection followed by eye detection
 * using OpenCV's built-in Haar cascade classifier. If the user's eye detected
 * successfully, an eye template is extracted. This template will be used in
 * the subsequent template matching for tracking the eye.
 */
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect.hpp"

cv::CascadeClassifier face_cascade;
cv::CascadeClassifier eye_cascade;

/**
 * Function to detect human face and the eyes from an image.
 *
 * @param  im    The source image
 * @param  tpl   Will be filled with the eye template, if detection success.
 * @param  rect  Will be filled with the bounding box of the eye
 * @return zero=failed, nonzero=success
 */
int detectEye(cv::Mat& im, cv::Mat& tpl, cv::Rect& rect)
{
    std::vector<cv::Rect> faces, eyes;
    eyes.clear();
    faces.clear();
    face_cascade.detectMultiScale(im, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, cv::Size(30,30));
    
    for (int i = 0; i < faces.size(); i++)
    {
        cv::Mat face = im(faces[i]);
        eye_cascade.detectMultiScale(face, eyes, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, cv::Size(20,20));
        
        if (eyes.size())
        {
            if (eyes[0].x < eyes[1].x)
            {
                rect = eyes[0] + cv::Point(faces[i].x, faces[i].y);
            }
            else
            {
                rect = eyes[1] + cv::Point(faces[i].x, faces[i].y);
            }
            tpl  = im(rect);
        }
    }
    
    return eyes.size();
}

/**
 * Perform template matching to search the user's eye in the given image.
 *
 * @param   im    The source image
 * @param   tpl   The eye template
 * @param   rect  The eye bounding box, will be updated with the new location of the eye
 */
void trackEye(cv::Mat& im, cv::Mat& tpl, cv::Rect& rect)
{
    cv::Size size(rect.width * 2, rect.height * 2);
    cv::Rect window(rect + size - cv::Point(size.width/2, size.height/2));
    
    window &= cv::Rect(0, 0, im.cols, im.rows);
    
    cv::Mat dst(window.width - tpl.rows + 1, window.height - tpl.cols + 1, CV_32FC1);
    cv::matchTemplate(im(window), tpl, dst, CV_TM_SQDIFF_NORMED);
    
    double minval, maxval;
    cv::Point minloc, maxloc;
    cv::minMaxLoc(dst, &minval, &maxval, &minloc, &maxloc);
    
    if (minval <= 0.2)
    {
        rect.x = window.x + minloc.x;
        rect.y = window.y + minloc.y;
    }
    else
        rect.x = rect.y = rect.width = rect.height = 0;
}

int main(int argc, char** argv)
{
    // Load the cascade classifiers
    // Make sure you point the XML files to the right path, or
    // just copy the files from [OPENCV_DIR]/data/haarcascades directory
    if (!face_cascade.load("haarcascade_frontalface_default.xml"))
    {
        printf("error face");
    }
    if (!eye_cascade.load("haarcascade_eye.xml"))
    {
        printf("error eye");
    }
    
    // Open webcam
    cv::VideoCapture cap(0);
    //scv::VideoCapture cap( "love6.mp4" );
    
    // Check if everything is ok
    if (face_cascade.empty() || eye_cascade.empty() || !cap.isOpened())
        return 1;
    
    // Set video to 320x240
    cap.set(CV_CAP_PROP_FRAME_WIDTH, 480);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, 360);
    
    cv::Mat frame, eye_tpl;
    cv::Rect eye_bb;
    
    while (1)
    {
        static bool start = false;
        
        cap >> frame;
        if (frame.empty() )//|| cv::waitKey(1) == 'q')
            break;
        
        // Flip the frame horizontally, Windows users might need this
        //cv::flip(frame, frame, 1);
        
        // Convert to grayscale and
        // adjust the image contrast using histogram equalization
        cv::Mat gray;
        cv::cvtColor(frame, gray, CV_BGR2GRAY);
        
        if ( cv::waitKey(15) == 's' )
        {
            if ( !start )
            {
                printf( "start\n" );
                detectEye(gray, eye_tpl, eye_bb);
            }
            else
            {
                printf( "stop\n" );
            }
            start = !start;
        }
        if ( !start )
        {
            // Display video
            cv::imshow("video", frame);
        }
        else
        {
            if ( eye_bb.width == 0 && eye_bb.height == 0 )
            {
                // Detection stage
                // Try to detect the face and the eye of the user
                detectEye(gray, eye_tpl, eye_bb);
                //printf( "deatect finish" );
            }
            else
            {
                // Tracking stage with template matching
                trackEye(gray, eye_tpl, eye_bb);
                //printf( "track finish" );
                eye_bb.x -= 75;
                eye_bb.width = 200;
                eye_bb.height = 20;
                
                // Draw bounding rectangle for the eye
                //if ( eye_bb.x - eye_bb.width / 2 > 0 && eye_bb.x + eye_bb.width / 2 < 320 )
                cv::rectangle(frame, eye_bb, CV_RGB(0,0,0), -1);
                // Display video
                cv::imshow("video", frame);
            }
        }
    }
    return 0;
}
/**
 * @file objectDetection2.cpp
 * @author A. Huaman ( based in the classic facedetect.cpp in samples/c )
 * @brief A simplified version of facedetect.cpp, show how to load a cascade classifier and how to find objects (Face + eyes) in a video stream - Using LBP here
 */
/*
#include "opencv2/objdetect.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

/** Function Headers */
/*void detectAndDisplay( Mat frame );

/** Global variables
String face_cascade_name = "haarcascade_frontalface_default.xml";
String eyes_cascade_name = "haarcascade_eye.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
String window_name = "Capture - Face detection";
/**
 * @function main
 
int main( void )
{
    VideoCapture capture;
    Mat frame;
    
    //-- 1. Load the cascade
    if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading face cascade\n"); return -1; };
    if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading eyes cascade\n"); return -1; };
    
    //-- 2. Read the video stream
    capture.open( -1 );
    if ( ! capture.isOpened() ) { printf("--(!)Error opening video capture\n"); return -1; }
    
    while ( capture.read(frame) )
    {
        if( frame.empty() )
        {
            printf(" --(!) No captured frame -- Break!");
            break;
        }
        
        //-- 3. Apply the classifier to the frame
        detectAndDisplay( frame );
        
        //-- bail out if escape was pressed
        int c = waitKey(10);
        if( (char)c == 27 ) { break; }
    }
    return 0;
}

/**
 * @function detectAndDisplay
 
void detectAndDisplay( Mat frame )
{
    std::vector<Rect> faces;
    Mat frame_gray;
    
    cvtColor( frame, frame_gray, COLOR_BGR2GRAY );
    equalizeHist( frame_gray, frame_gray );
    
    //-- Detect faces
    face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0, Size(80, 80) );
    
    for( size_t i = 0; i < faces.size(); i++ )
    {
        Mat faceROI = frame_gray( faces[i] );
        std::vector<Rect> eyes;
        
        //-- In each face, detect eyes
        eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CASCADE_SCALE_IMAGE, Size(30, 30) );
        if( eyes.size() >= 1)
        {
            //-- Draw the face
            Point center( faces[i].x + faces[i].width/2, faces[i].y + faces[i].height/2 );
            ellipse( frame, center, Size( faces[i].width/2, faces[i].height/2 ), 0, 0, 360, Scalar( 255, 0, 0 ), 2, 8, 0 );
            
            for( size_t j = 0; j < eyes.size(); j++ )
            { //-- Draw the eyes
                Point eye_center( faces[i].x + eyes[j].x + eyes[j].width/2, faces[i].y + eyes[j].y + eyes[j].height/2 );
                int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
                circle( frame, eye_center, radius, Scalar( 255, 0, 255 ), 3, 8, 0 );
            }
        }
        
    }
    //-- Show what you got
    imshow( window_name, frame );
}
*/